<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Login.
     *
     * @param LoginRequest $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $imageName = $request->all()['name_file'];
 
        $uploadImage = $request->image->move(public_path('images'), $imageName);

        if ($uploadImage) {
            return $imageName;
        }
    }
}
